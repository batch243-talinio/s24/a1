// console.log('Zawarudo!')

const num = 2;
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`)

let arrayAddress = ["245 Washington Ave NW", "California", "20053"];

let [cityAddress, stateAddress, codeAddress] = arrayAddress;
console.log(`I live at ${cityAddress}, ${stateAddress} ${codeAddress}`)

const animal = {
	name: 'Lolong',
	species: 'salt water crocodile',
	weight: '1075 kgs',
	measurement: '20ft 3in'
};

let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weight at ${weight} with a measurement of ${measurement}`)

const arrayNumbers = [2,4,6,8,10];
arrayNumbers.forEach((arrayNumbers) => console.log(`${arrayNumbers}`));

const reduceNumber = arrayNumbers.reduce(
		(accumulator, currentValue) => accumulator + currentValue
);
console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
	}
}

let dawg = new Dog("Fall", "4" , "American Bully");
console.log(dawg);
